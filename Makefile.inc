# Install directory prefixes.
INSTALL_DIR=../lheh5-install
HDF5_DIR = /usr/local/Cellar/hdf5/1.10.6
HIFIVE_DIR = ../HighFive
MPICH_DIR =/usr/local/Cellar/mpich/3.3.2

CXX=/usr/local/bin/mpicxx

PREFIX_BIN=$(INSTALL_DIR)/bin
PREFIX_INCLUDE=$(INSTALL_DIR)/include
PREFIX_LIB=$(INSTALL_DIR)/lib
PREFIX_SHARE=$(INSTALL_DIR)/share/

LIB_COMMON = -L$(HDF5_DIR)/lib -lhdf5_hl -lhdf5_cpp -lhdf5

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=true
CXX_COMMON = -I$(HDF5_DIR)/include -I$(HIFIVE_DIR)/include -I$(MPICH_DIR)/include/ -std=c++11

CXX_SHARED=-dynamiclib
CXX_SONAME=-Wl,-dylib_install_name,@rpath/
LIB_SUFFIX=.dylib

